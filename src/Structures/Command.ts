/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Eris, { CommandClient, CommandOptions, Message, TextChannel } from 'eris';
import Client from './Client';

interface MCommandOptions extends CommandOptions {
    hooks?: {
        preCommand?: (msg: Message, args: string[] ) => { msg?: Message; args?: string[] } | void;
        postCheck?: (msg: Message, args: string[], checksPassed: boolean) => void;
        postExecution?: (msg: Message, args: string[], executionSuccess: boolean) => void;
        postCommand?: (msg: Message, args: string[], sent?: Message) => void;
        aPreCommand?: (msg: Message, args: string[] ) => { msg?: Message; args?: string[] } | void;
    };
}

export default class Command {
    public client: CommandClient;

    public label: string;

    public aliases: string[];

    public options: MCommandOptions;

    public subcommands: Command[];

    public asubcommands: Command[];

    public permissionsNeeded: string[];

    public base?: Client;

    public ownerOnly?: boolean;

    public path: string;

    public command?: Eris.Command;

    constructor(client: CommandClient, base?: Client) {
        this.client = client;
        this.base = base;
        this.label = 'label';
        this.aliases = [];
        this.options = {};
        this.subcommands = [];
        this.asubcommands = [];

        this.permissionsNeeded = [];
        this.path = __filename;
    }

    handleArgs(msg: Message) {
        if (msg.prefix) {
            const _args = msg.content.replace(/<@!/g, '<@').substring(msg.prefix.length).trim()
                .split(/[ \f\r\t\v\u00a0\u1680\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]/g);
            _args.shift();
            return { args: _args };
        }
        const _args = msg.content.replace(/<@!/g, '<@').trim()
            .split(/[ \f\r\t\v\u00a0\u1680\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]/g);
        _args.shift();
        return { args: _args };
    }

    handleOptions(): void {
        if (this.ownerOnly && this.base && this.base.config.owner && this.base.config.owner.discordID) {
            if (!this.options.requirements) {
                this.options.requirements = {
                    userIDs: [this.base.config.owner.discordID],
                };
            } else if (this.options.requirements && !this.options.requirements.userIDs) {
                this.options.requirements.userIDs = [this.base.config.owner.discordID];
            } else if (this.options.requirements.userIDs) {
                if (!Array.isArray(this.options.requirements.userIDs) ) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                    // @ts-ignore
                    this.options.requirements.userIDs = [this.options.requirements.userIDs, this.base.config.owner.discordID];
                } else if (Array.isArray(this.options.requirements.userIDs) && !this.options.requirements.userIDs.includes(this.base.config.owner.discordID) ) {
                    this.options.requirements.userIDs.push(this.base.config.owner.discordID);
                }
            }
        }
        if (!this.options.hooks) {
            this.options.hooks = {
                preCommand: this.handleArgs,
            };
        } else if (this.options.hooks.preCommand || this.options.hooks.aPreCommand) {
            if (this.options.hooks.preCommand) {
                this.options.hooks.aPreCommand = this.options.hooks.preCommand;
            }
            this.options.hooks.preCommand = (msg: Message): { msg?: Message; args?: string[] } | void => {
                const { args } = this.handleArgs(msg);
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                const out: { args?: string[]; message?: Message } = this.options.hooks.aPreCommand(msg, args);
                if (!out) {
                    return { args };
                }
                if (!out.args) {
                    out.args = args;
                }
                return out;
            };
        } else {
            this.options.hooks.preCommand = this.handleArgs;
        }
    }

    canRegister(): boolean | Promise<boolean> {
        return !(this.ownerOnly && (!this.base || !this.base.config.owner || !this.base.config.owner.discordID) );
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    execute(msg: Message, args: string[] ): Promise<void|Message|string> | void|Message|string {
        throw Error('EXECUTE IS ABSTRACT FUNCTION');
    }

    _execute(msg: Message, args: string[] ): boolean | any {
        if (this.ownerOnly && (!this.base || !this.base.config.owner || !this.base.config.owner.discordID || this.base.config.owner.discordID !== msg.author.id) ) {
            return 'No.';
        }
        if (this.permissionsNeeded && this.permissionsNeeded.length > 0 && !(msg.channel instanceof TextChannel) ) {
            return 'No.';
        }
        if (this.permissionsNeeded && this.permissionsNeeded.length > 0) {
            let success = true;
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            const botmem = msg.channel.guild.members.get(this.client.user.id);
            for (const permission of this.permissionsNeeded) {
                if (!botmem.permission.has(permission) ) {
                    success = false;
                    break;
                }
            }
            if (!success) {
                return 'No.';
            }
        }
        return this.execute(msg, args);
    }
}
