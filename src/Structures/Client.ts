/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CommandClient } from 'eris';
import * as Commands from '../Commands/';
import CommandStruc from './Command';
import { promisify } from 'util';
import { join } from 'path';

const sleep = promisify(setTimeout);

interface Config {
    url?: string;
    token: string;
    prefix?: string[] | string;
    owner?: { discordID: string; tokenAuth?: string };
    showUrl?: boolean;
}

export default class Client {
    public client?: CommandClient;

    public config: Config;

    public commands: Map<string, CommandStruc>;

    constructor(config: Config) {
        this.config = this.genConfig(config);

        this._onReady = this._onReady.bind(this);
        this.commands = new Map();
    }

    genConfig(config: Config): Config {
        if (!config) {
            throw Error('[CONFIG} - Config is missing');
        }
        if (!config.token) {
            throw Error('[CONFIG] - Missing token!');
        }
        return config;
    }

    reregisterCommand(command: CommandStruc): boolean {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        this.client.unregisterCommand(command.label);

        const exc = command.execute.bind(command);
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        const cmd = this.client.registerCommand(command.label, exc, command.options);
        command.command = cmd;
        this.commands.set(command.label, command);
        if (command.subcommands) {
            for (let Subcommand of command.subcommands) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                Subcommand = new Subcommand(this.client, this);
                const exc = Subcommand._execute.bind(Subcommand);
                command.asubcommands.push(Subcommand);
                Subcommand.command = cmd.registerSubcommand(Subcommand.label, exc, Subcommand.options);
            }
        }
        return true;
    }

    initCommand(ICommand: any): void {
        const cmdName = ICommand;
        console.log(`[COMMAND - LOADING] - ${cmdName}`);
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        ICommand = Commands[ICommand];
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        const command = new ICommand(this.client, this);
        command.path = join(__dirname, `../Commands/${cmdName}.js`);
        if (!command.label) {
            throw Error(`[COMMAND FAIL] - Command ${cmdName} is missing label!`);
        }
        command.handleOptions();
        const canExecute = command.canRegister();
        if (!canExecute) {
            console.log(`[COMMAND - LOAD SKIP] - ${cmdName}`);
            return;
        }
        const exc = command._execute.bind(command);
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        const cmd = this.client.registerCommand(command.label, exc, command.options);
        command.command = cmd;
        this.commands.set(command.label, command);
        if (command.aliases) {
            for (const alias of command.aliases) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                this.client.registerCommandAlias(alias, command.label);
            }
        }
        if (command.subcommands) {
            for (let Subcommand of command.subcommands) {
                Subcommand = new Subcommand(this.client, this);
                const exc = Subcommand._execute.bind(Subcommand);
                command.asubcommands.push(Subcommand);
                Subcommand.command = cmd.registerSubcommand(Subcommand.label, exc, Subcommand.options);
                if (Subcommand.aliases) {
                    for (const alias of Subcommand.aliases) {
                        cmd.registerSubcommandAlias(alias, Subcommand.label);
                    }
                }
            }
        }
        console.log(`[COMMAND] - Loaded command ${command.label} (${cmdName})`);
    }

    async initCommands(): Promise<void|boolean> {
        if (!this.client) {
            await sleep(1000);
            return this.initCommands();
        }
        for (const Command in Commands) {
            this.initCommand(Command);
        }
        return true;
    }

    _onReady(): void {
        this.initCommands();
        console.log('[CLIENT] - Ready!');
    }

    async init(): Promise<void|boolean> {
        if (!this.config) {
            await sleep(1000);
            return this.init();
        }
        console.log('Initializing...');
        let prefix = ['evx', '@mention'];
        if (this.config.prefix && Array.isArray(this.config.prefix) ) {
            prefix = ['@mention'].concat(this.config.prefix);
        } else if (this.config.prefix) {
            prefix = ['@mention', this.config.prefix];
        }
        this.client = new CommandClient(this.config.token, { restMode: true, defaultImageFormat: 'png', defaultImageSize: 512 }, { defaultHelpCommand: false, prefix, defaultCommandOptions: { cooldown: 3000 } } );
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        this.client.on('ready', this._onReady);

        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        await this.client.connect();
        return false;
    }
}
