/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import { CommandClient, Message } from 'eris';
import Client from '../Structures/Client';

export default class BotInfo extends Command {
    constructor(client: CommandClient, base: Client) {
        super(client, base);
        this.label = 'botinfo';
        this.aliases = ['bi'];
        this.options = { description: 'Give bot related info', fullDescription: 'Give some information for EVX-Bot' };
    }

    execute(msg: Message): Promise<void | Message | string> | void | Message | string {
        return msg.channel.createMessage( {
            embed: {
                title: 'Info on EVX-Bot',
                description: 'A simple bot for interfacing with a Evolve-X instance, no database required.\nRequires access to a Evolve-X instance',
                fields: [
                    {
                        name: 'Gitlab',
                        value: 'https://gitlab.com/VoidNulll/EVX-Bot',
                    },
                    {
                        name: 'License',
                        value: 'AGPL Version 3.0 or later',
                    },
                    {
                        name: 'Made by',
                        value: 'VoidNulll (https://gitlab.com/VoidNulll)',
                    },
                ],
                color: 0x15959D,
            },
        } );
    }
}
